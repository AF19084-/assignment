import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton gyouzaButton;
    private JButton checkOutButton;
    private JLabel total;
    int sum=0;

    void order(String food, int fee){
        String currentText = textPane1.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to oder " + food,
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Order for "+ food +" received!");
            textPane1.setText(currentText + food + " " + fee + "yen\n");
            sum += fee;
            total.setText("Total " + sum + " yen");
        }
    }

    public FoodGUI() {
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("templa.jpg")
        ));
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")
        ));
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakisoba.jpg")
        ));
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")
        ));
        gyouzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyouza.jpg")
        ));

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 100);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 150);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 200);
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza", 300);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", 400);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 500);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                }
                confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you want to eat in",
                        "place Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. Total price is " + sum + "yen");
                    sum = 0;
                    textPane1.setText("");
                    total.setText("total 0 yen");
                }
                else{
                    confirmation = JOptionPane.showConfirmDialog(null,
                            "Do you need a bag ?\nBag is 2 yen",
                            "bag Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if(confirmation == 0){
                        sum+=2;
                        JOptionPane.showMessageDialog(null, "Thank you. Total price is " + sum + "yen");
                        sum = 0;
                        textPane1.setText("");
                        total.setText("total 0 yen");
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Thank you. Total price is " + sum + "yen");
                        sum = 0;
                        textPane1.setText("");
                        total.setText("total 0 yen");
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
